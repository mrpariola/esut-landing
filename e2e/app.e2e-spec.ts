import { EsutLandingPage } from './app.po';

describe('esut-landing App', function() {
  let page: EsutLandingPage;

  beforeEach(() => {
    page = new EsutLandingPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
